package ru.clevertec.collection;

import java.util.*;

/**
 * Resizable-array implementation of the Deque interface. Array deques have no capacity restrictions; they grow as necessary to support usage. They are not thread-safe; in the absence of external synchronization, they do not support concurrent access by multiple threads. Null elements are prohibited. This class is likely to be faster than Stack when used as a stack, and faster than LinkedList when used as a queue.
 * @author Andrey Skibunov
 * @version 1.0 release
 */

public class SimpleDeque<E> extends AbstractCollection<E> implements Deque<E> {

    private static final int DEFAULT_CAPACITY = 16;

    private int head;

    private int tail;

    private Object[] elements;

    private <T> T[] copyElements(T[] a) {
        if (head < tail) {
            System.arraycopy(elements, head, a, 0, size());
        } else if (head > tail) {
            int headPortionLen = elements.length - head;
            System.arraycopy(elements, head, a, 0, headPortionLen);
            System.arraycopy(elements, 0, a, headPortionLen, tail);
        }
        return a;
    }

    private void doubleCapacity() {
        assert head == tail;
        int p = head;
        int n = elements.length;
        int r = n - p; // number of elements to the right of p
        int newCapacity = n * 2;
        if (newCapacity < 0)
            throw new IllegalStateException("Sorry, queue too big");
        Object[] a = new Object[newCapacity];
        System.arraycopy(elements, p, a, 0, r);
        System.arraycopy(elements, 0, a, r, p);
        elements = a;
        head = 0;
        tail = n;
    }

    private <T> void delete(int index) {
        T[] a = (T[]) new Object[elements.length];
        System.arraycopy(elements, 0, a, 0, index);
        System.arraycopy(elements, index + 1, a, index, tail - index);
        tail = (tail - 1) & (elements.length - 1);
        elements = a;
    }

    /**
     * Returns the number of elements in this deque.
     * @return the number of elements in this deque
     */
    @Override
    public int size() {
        return (tail - head) & (elements.length - 1);
    }

    public SimpleDeque() {
        elements = new Object[DEFAULT_CAPACITY];
    }

    /**
     * Inserts the specified element at the end of this queue.
     * @param e
     * @return if element insert return true, else return false
     * @throws NullPointerException if the specified element is null
     */
    @Override
    public boolean add(E e) {
        addLast(e);
        return true;
    }

    /**
     * Inserts the specified element at the front of this deque.
     * @param e the element to add
     * @throws NullPointerException if the specified element is null
     */
    @Override
    public void addFirst(E e) {
        if (e == null)
            throw new NullPointerException("Element is null");
        head = (head - 1) & (elements.length - 1);
        elements[head] = e;
        if (head == tail)
            doubleCapacity();
    }

    /**
     * Inserts the specified element at the end of this deque.
     * @param e the element to add
     * @throws NullPointerException if the specified element is null
     */
    @Override
    public void addLast(E e) {
        if (e == null)
            throw new NullPointerException("Element is null");
        elements[tail] = e;
        tail = (tail + 1) & (elements.length - 1);
        if (tail == head)
            doubleCapacity();
    }

    /**
     * Retrieves, but does not remove, the first element of this deque.
     * @return First elements from deque
     * @throws NoSuchElementException if the specified element is null
     */
    @Override
    public E getFirst() {
        E result = (E) elements[head];
        if (result == null)
            throw new NoSuchElementException();
        return result;
    }

    /**
     * Retrieves, but does not remove, the last element of this deque.
     * @return Last elements from deque
     * @throws NoSuchElementException if the specified element is nul
     */
    @Override
    public E getLast() {
        E result = (E) elements[(tail - 1) & (elements.length - 1)];
        if (result == null)
            throw new NoSuchElementException();
        return result;
    }

    /**
     * Inserts the specified element at the end of this queue.
     * @param e
     * @return if insert return true else false
     */
    @Override
    public boolean offer(E e) {
        offerLast(e);
        return true;
    }

    /**
     * Inserts the specified element at the front of this deque.
     * @param e
     * @return if insert return true else false
     */
    @Override
    public boolean offerFirst(E e) {
        if (e == null)
            return false;
        head = (head - 1) & (elements.length - 1);
        elements[head] = e;
        if (tail == head)
            doubleCapacity();
        return true;
    }

    /**
     * Inserts the specified element at the end of this deque.
     * @param e
     * @return if insert return true else false
     */
    @Override
    public boolean offerLast(E e) {
        if (e == null)
            return false;
        elements[tail] = e;
        tail = (tail + 1) & (elements.length - 1);
        if (tail == head)
            doubleCapacity();
        return true;
    }

    /**
     * Retrieves, but does not remove, the head of the queue represented by this queue.
     * @return First elements from deque
     * @throws NoSuchElementException if the specified element is null
     */
    @Override
    public E element() {
        return getFirst();
    }

    /**
     * Retrieves, but does not remove, the head of the queue represented by this deque.
     * @return returns null if this deque is empty else returns head elements from deque
     */
    @Override
    public E peek() {
        return peekFirst();
    }

    /**
     * Retrieves, but does not remove, the first element of this deque.
     * @return returns null if this deque is empty else returns head elements from deque
     */
    @Override
    public E peekFirst() {
        return (E) elements[head];
    }

    /**
     * Retrieves, but does not remove, the last element of this deque.
     * @return returns null if this deque is empty else returns last elements from deque
     */
    @Override
    public E peekLast() {
        return (E) elements[(tail - 1) & (elements.length - 1)];
    }

    /**
     * Retrieves and removes the head of the queue represented by this deque.
     * @return the head of the queue represented by this deque, or null if this deque is empty
     */
    @Override
    public E poll() {
        return pollFirst();
    }

    /**
     * Retrieves and removes the first element of this deque.
     * @return the first element of the queue represented by this deque, or null if this deque is empty.
     */
    @Override
    public E pollFirst() {
        E result = (E) elements[head];
        if (result == null)
            return null;
        elements[head] = null;
        head = (head + 1) & (elements.length - 1);

        return result;
    }

    /**
     * Retrieves and removes the last element of this deque.
     * @return the last element of the queue represented by this deque, or null if this deque is empty.
     */
    @Override
    public E pollLast() {
        E result = (E) elements[(tail - 1) & (elements.length - 1)];
        if (result == null)
            return null;
        elements[((tail - 1) & (elements.length - 1))] = null;
        tail = (tail - 1) & (elements.length - 1);
        return result;
    }

    /**
     * Pops an element from the stack represented by this deque.
     * @return the element at the front of this deque (which is the top of the stack represented by this deque)
     * @throws NoSuchElementException if the specified element is null
     */
    @Override
    public E pop() {
        return removeFirst();
    }

    /**
     * Inserts the specified element at the front of this deque.
     * @param e the element to add
     * @throws NullPointerException if the specified element is null
     */
    @Override
    public void push(E e) {
        addFirst(e);
    }

    /**
     * Retrieves and removes the head of the queue represented by this deque.
     * @return the head of the queue represented by this deque, or null if this deque is empty
     * @throws NullPointerException if the specified element is null
     */
    @Override
    public E remove() {
        return removeFirst();
    }

    /**
     * Removes a single instance of the specified element from this deque.
     * @param o
     * @return the head of the queue represented by this deque, or null if this deque is empty
     * @throws NullPointerException if the specified element is null
     */
    @Override
    public boolean remove(Object o) {
        return removeFirstOccurrence(o);
    }

    /**
     * Retrieves and removes the first element of the queue represented by this deque.
     * @return the first element of the queue represented by this deque, or null if this deque is empty
     * @throws NullPointerException if the specified element is null
     */
    @Override
    public E removeFirst() {
        E result = (E) elements[head];
        if (result == null)
            throw new NullPointerException("removeFirst Element is null");
        elements[head] = null;
        head = (head + 1) & (elements.length - 1);

        return result;
    }

    /**
     * Retrieves and removes the last element of the queue represented by this deque.
     * @return the last element of the queue represented by this deque, or null if this deque is empty
     * @throws NullPointerException if the specified element is null
     */
    @Override
    public E removeLast() {
        E result = (E) elements[(tail - 1) & (elements.length - 1)];
        if (result == null)
            throw new NullPointerException("removeLast Element is null");
        elements[(tail - 1) & (elements.length - 1)] = null;
        tail = (tail - 1)  & (elements.length - 1);
        return result;
    }

    /**
     * Removes the first occurrence of the specified element in this deque (when traversing the deque from head to tail).
     * @param o
     * @return true if the deque contained the specified element
     */
    @Override
    public boolean removeFirstOccurrence(Object o) {
        if (o == null)
            return false;

        int h = head & (elements.length - 1);
        int t = (tail - 1)  & (elements.length - 1);
        while (h <= t) {
            if (elements[h].equals(o)) {
                delete(h);
                return true;
            }
            h = (h + 1) & (elements.length - 1);;
        }
        return false;
    }

    /**
     * Removes the last occurrence of the specified element in this deque (when traversing the deque from head to tail).
     * @param o
     * @return true if the deque contained the specified element
     */
    @Override
    public boolean removeLastOccurrence(Object o) {
        if (o == null)
            return false;

        int h = head & (elements.length - 1);
        int t = (tail - 1)  & (elements.length - 1);

        while (h <= t) {
            if (elements[t].equals(o)) {
                delete(t);
                return true;
            }
            t = (t - 1) & (elements.length - 1);;
        }
        return false;
    }

    /**
     * Returns true if this deque contains no elements.
     * @return true if this deque contains no elements
     */
    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * Removes all of the elements from this deque.
     */
    @Override
    public void clear() {
        int h = head;
        int t = tail;

        while (h != t) {
            elements[h] = null;
            h = (h + 1) & (elements.length - 1);
        }
        head = 0;
        tail = 0;
    }

    /**
     * Returns true if this queue contains the specified element.
     * @param o
     * @return true if this deque contains the specified element
     */
    @Override
    public boolean contains(Object o) {
        if (o == null)
            return false;

        int h = head;
        int t = tail;

        while (h <= t) {
            if (elements[h].equals(o))
                return true;
            h = (h + 1) & (elements.length - 1);
        }
        return false;
    }

    /**
     * Returns an array containing all of the elements in this deque in proper sequence (from first to last element).
     * @return an array containing all of the elements in this deque
     */
    @Override
    public Object[] toArray() {
        return copyElements(new Object[size()]);
    }


    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public Iterator<E> descendingIterator() {
        return null;
    }


    @Override
    public String toString() {
        return Arrays.toString(toArray());
    }
}
