package ru.blisse;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import ru.clevertec.collection.SimpleDeque;

import java.util.Deque;
import java.util.NoSuchElementException;

public class SimpleDequeTest {

    private Deque simpleDeque = mock(SimpleDeque.class);

    @Test
    public void addTest(){
        Mockito.when(simpleDeque.add(anyInt())).thenReturn(true);
        assertEquals(true,simpleDeque.add(10));
    }

    @Test(expected = NullPointerException.class)
    public void addTestException(){
        Mockito.when(simpleDeque.add(null)).thenThrow(new NullPointerException());
        assertEquals(new NullPointerException(),simpleDeque.add(null));
    }


    @Test
    public void getFirstTest(){
        Mockito.when(simpleDeque.getFirst()).thenReturn(1);
        assertEquals(1,simpleDeque.getFirst());
    }

    @Test
    public void getLastTest(){
        Mockito.when(simpleDeque.getLast()).thenReturn(10);
        assertEquals(10,simpleDeque.getLast());
    }

    @Test
    public void offerTest(){
        Mockito.when(simpleDeque.offer(anyInt())).thenReturn(true);
        assertEquals(true,simpleDeque.offer(10));

        Mockito.when(simpleDeque.offer(null)).thenReturn(false);
        assertEquals(false,simpleDeque.offer(null));
    }

    @Test
    public void elementTest(){
        Mockito.when(simpleDeque.element()).thenReturn(1);
        assertEquals(1,simpleDeque.element());
    }

    @Test(expected = NoSuchElementException.class)
    public void elementTestException(){
        Mockito.when(simpleDeque.element()).thenThrow(new NoSuchElementException());
        assertEquals(new NoSuchElementException(),simpleDeque.element());
    }

    @Test
    public void peekTest(){
        Mockito.when(simpleDeque.peek()).thenReturn(1);
        assertEquals(1,simpleDeque.peek());

        Mockito.when(simpleDeque.peek()).thenReturn(null);
        assertEquals(null,simpleDeque.peek());
    }

    @Test
    public void peekLastTest(){
        Mockito.when(simpleDeque.peekLast()).thenReturn(10);
        assertEquals(10,simpleDeque.peekLast());

    }

    @Test
    public void pollTest(){
        Mockito.when(simpleDeque.poll()).thenReturn(1);
        assertEquals(1,simpleDeque.poll());

        Mockito.when(simpleDeque.poll()).thenReturn(null);
        assertEquals(null,simpleDeque.poll());

    }

    @Test
    public void pollLastTest(){
        Mockito.when(simpleDeque.pollLast()).thenReturn(10);
        assertEquals(10,simpleDeque.pollLast());
    }

    @Test
    public void removeTest(){
        Mockito.when(simpleDeque.remove()).thenReturn(1);
        assertEquals(1,simpleDeque.remove());
    }

    @Test(expected = NoSuchElementException.class)
    public void removeTestException(){
        Mockito.when(simpleDeque.remove()).thenThrow(new NoSuchElementException());
        assertEquals(new NoSuchElementException(),simpleDeque.remove());
    }

    @Test
    public void removeLastTest(){
        Mockito.when(simpleDeque.removeLast()).thenReturn(10);
        assertEquals(10,simpleDeque.removeLast());
    }

    @Test(expected = NoSuchElementException.class)
    public void removeLastTestException(){
        Mockito.when(simpleDeque.removeLast()).thenThrow(new NoSuchElementException());
        assertEquals(new NoSuchElementException(),simpleDeque.removeLast());
    }

    @Test
    public void containsTest(){
        Mockito.when(simpleDeque.contains(anyInt())).thenReturn(true);
        assertEquals(true,simpleDeque.contains(10));

        Mockito.when(simpleDeque.contains(anyInt())).thenReturn(false);
        assertEquals(false,simpleDeque.contains(1));
    }


    @Test
    public void popTest(){
        Mockito.when(simpleDeque.pop()).thenReturn(1);
        assertEquals(1,simpleDeque.pop());
    }

    @Test
    public void pushTest(){
        doNothing().when(simpleDeque).push(anyInt());
        simpleDeque.push(1);
        verify(simpleDeque, times(1)).push(1);
    }

    @Test
    public void sizeTest(){
        Mockito.when(simpleDeque.size()).thenReturn(5);
        assertEquals(5,simpleDeque.size());
    }

    @Test
    public void isEmptyTest(){
        Mockito.when(simpleDeque.isEmpty()).thenReturn(true);
        assertEquals(true,simpleDeque.isEmpty());

        Mockito.when(simpleDeque.isEmpty()).thenReturn(false);
        assertEquals(false,simpleDeque.isEmpty());
    }

}
